




Maricel Chavarría, 
Barcelona



		05/06/2020 09:35

 |  Actualizado a 05/06/2020 23:09
 
La Orquestra Simfònica de Barcelona i Nacional de Catalunya tendrá ocho directoras invitadas la próxima temporada, si la pandemia del coronavirus lo permite. Entre ellas, la francesa Lawrence Equilbey, conocida por sus proyectos avanzados con la parisina Insula Orchestra, o la alemana Anja Bihlamaier, que hace una carrera nórdica y este año asume el liderazgo de la Residentie Orkest en La Haya. 
Lo que prometió el director de L’Auditori, cuando hace 17 meses asumió el cargo, comienza a tomar forma. “En cinco años ha de haber por lo menos un 50% de mujeres dirigiendo la OBC”, dijo entonces Robert Brufau, consciente de que el porcentaje de compositoras y de batutas mujeres en las programaciones de la clásica es uno de los escollos de la igualdad en la cultura.







Medidas de seguridad este verano
La orquesta aprovechará el ampio escenario de L’Auditori para tocar con menos de 50 músicos y a distancia de seguridad


De momento la orquesta barcelonesa se incorpora el día 15 de junio al trabajo. “Tenemos uno de los escenarios de auditorio más grandes del país y lo que haremos será intentar separar bien a los músicos, situar los vientos en los balcones, por ejemplo. El planteamiento es ir viendo y ser prudentes, estamos alineados con las salas europeas que forman parte de ECHO y la idea es trabajar con un máximo de 50 músicos y hacer cosas de cámara, sin test porque serían demasiados, pero sí tomando la temperatura y haciendo control de síntomas”, explica Brufau.
Durante seis semanas trabajarán un programa a puerta cerrada de lunes a jueves, y los viernes grabarán para emitir por Catalunya Música y TV3, al igual que la Banda Municipal en la Sala 3. Sin público en la sala. “Es un preludio de lo que puede llegar a suceder en otoño, en caso de que no se pueda abrir con normalidad y estuviéramos con distanciamiento entre músicos y entre el público”, añade el director de la sala. 





							Arnau y Abel Tomàs, Vera Martínez Mehner y Jonathan Brown, los integrantes del referencial Quartet Casals que actuará en septiembre, dentro de un nuevo proyecto de L'Auditorii: la Biennal de Quartets de Barcelona
										(David Ruano)
						










Para esta situación hay pensado un plan B, sustituyendo por ejemplo una Sinfonía Turangalila de Messiaen o un Fidelio de Beethoven por música más de cámara. Y en este proyecto se rescataría el estreno de Pablo Carrascosa (White) que quedó cancelada y se haría en streaming. O a Xavi Torres revisitando en clave de jazz las sonatas de Beethoven. Y se cuenta con la complicidad del Quartet Casals, de Jordi Savall, y de la OBC y la Banda… “Más de 40 producciones están previstas en el plan B”.
Preocupado por los 9.000 abonados de la sala –“algo habrá que ofrecerles”–, Brufau ha dispuesto que la renovación para la próxima temporada se pueda efectuar a partir de hoy mismo, pero no se cobrará hasta septiembre, “cuando se resuelvan las condiciones para abrir y nos adaptemos a la realidad existente”. Además anuncia abonos híbridos, para seguir presencial o digitalmente la programación, “La estaremos ofreciendo toda en streaming a través de auditorinacional.cat, porque puede haber gente que sienta que no pueda arriesgarse a venir”. 


Novedades
La temporada abrirá con la primera Biennal de Quartets de Barcelona, comisariada por los Casals, y cerrará con el Festival Mozart Nits d’Estiu


El plan A, esto es, la temporada que tiene prevista L’Auditori, apunta diversas novedades. Dos nuevos festivales abrirán y clausurarán el curso: la primera Biennal Internacional de Quartets de Barcelona y el Festival Mozart Nits d’Estiu. El primero, comisariado por el Quartet Casals, quiere ser un referente europeo, con diez conciertos de diez cuartetos en cuatro días, entre los que, a parte de los grandes como Modigliani o Artemis, haya cuatro de nacionales, además de los propios Casals. Lo cual evidenciará su gran legado.










							El clarinetista Andreas Ottensamer participará del festival Mozart Nits d'Estiu en julio del 2021
										(Ludwig Reiter)
						




Con el festival Mozart Nits d’Estiu, inspirado en el que se hacía hace años, se quiere dar vida a la orquesta más allá de la temporada, en julio.”La sacaremos de L’Auditori, estará una semana, sí, pero otra en el Palau de la Música Catalana, y otra en el Tinell, haciendo obras de Mozart y del clasicismo. Una forma de recuperar las bases del sinfonismo para trabajar”. Se invitará sólo a batutas con criterios historicistas, entre los que se encuentra la citada Lawrence Equilbey, Jonathan Cohen –con el clarinetista Andreas Ottensamer– y el consagrado Andrea Marcon



Coro profesional
Nace el Cor de L’Auditori, que dirigido por Xavier Pastrana participará en cuatro conciertos este curso


Y otra novedad: nace el Cor de L’Auditori, una formación profesional que participará en cuatro conciertos esta temporada, dirigida por Xavier Pastrana. “Contamos con coros amateurs extraordinarios y podemos formar uno profesional con gente de aquí, en colaboración otros importantes, como el Cor de Cambra del Palau o el Cor Jove Nacional de Catalunya “, señala Brufau, quien planea titularidades cortas, de tres años. Y destaca un proyecto a capella sobre Britten.










							La directora de orquesta Laurence Equilbey, una de las ocho que dirigirán la OBC
										(Jean-Baptiste Millot)
						




Como ya apuntó La Vanguardia antes de la pandemia, la sala ha creado un sello discográfico digital y una plataforma de video bajo demanda que ofrecerá conciertos en streaming. “La Covid-19 nos pilló en medio de este proceso. Está centrado en el patrimonio, y proveerá pues desdel e disco que era preestreno de la Banda Municipal hasta trabajos de la OBC con Lamote de Grignon, Mompou, Blancafort, Martínez Valls...


Sello propio
L a sala crea un sello discográfico digital centrado en el patrimonio y una plataforma de video bajo demanda para conciertos en streaming


La temporada contará con 58 obras de patrimonio, de todas las épocas y estilos, pero centrada en dos aniversarios: Gerhard y Manén. Y dedican la gran exposición al centenario de la Orquestra Pau Casals, reivindicando los autores que llegaron a interpretar.l





							Thomas Adès, compositor invitado en la temporada 2020-21 de L'Auditori.
										(Marco Borggreve / ACN)
						










Y hablando de autores, esta temporada, cuyo leit motive es la creación, contará con dos grandes nombres de la actualidad como compositores invitados: Thomas Adès (Londres, 1971), con su mirada surreal, y Cassandra Miller (Metchosin, Canadà, 1974), que precisamente huye de la creación a partir de la nada. Su obra es un constante eco de piezas ya existentes, que diluye hasta hacerlas imperceptibles. De ella se escuchará por ejemplo About Bach para cuarteto de cuerda o un estreno mundial de una obra sinfónica, encargo de L’Auditori.


Grandes nombres actuales
Thomas Adès y Cassandra Miller son compositores invitados en una temporada cuyo leit motive es la creación


Miller forma parte del impulso que quiere dar el equipamiento a las creadoras, en todos los ciclos de su temporada, además de a las directoras, entre las que también están la polaca Marta Gardolińska, la griega Zoi Tsokanou, la lituana Giedrė Šlekytė, la alemana Ruth Reinhardt (colega de Dudamel en la Filarmónica de Los Angeles), la china Elim Chan y la surcoreana Shiyeon Sung.

Y a parte de Kazushi Ono, como director titular, y Jan Willem de Vriend como principal invitado, dirigirán la OBC las batutas de Nuno Coelho, Matthias Pintscher, Luovic Morlot, Nacho de Paz, Duncan Ward, Francesc Prat, Josep Caballé Domènech y Vasily Petrenoko.










							Vasily Petrenko, director de orquesta
										(Tarlova)
						




Y en cuanto al repertorio, habrá hasta 36 estrenos de obras nuevas habrá esta próxima temporada, con encargos específicos: desde Jordi Cervelló, Albert Sardà o Josep Maria Guix hasta Raquel García-Tomás, Núria Giménez Comas o Miquel Oliu.
“Como equipamiento púbico tenemos que ser complementarios con las otras dos grandes salas de la ciudad, para dejar claro quiénes somos. El reflejo de la diversidad musical del siglo XXI es nuestra nueva identidad artística”, asegura Robert Brufau.





Personalidad
El reflejo de la diversidad musical del siglo XXI es nuestra nueva identidad artística”




Robert Brufau
Director de L’Auditori


Y se revisitará mitos fundacionales del origen o la génesis como el oratorio La creación de Haydn, Así habló Zaratustra de Richard Strauss,
 In Seven Days de Thomas Adès, Popol Vuh d’Alberto Ginastera o el estreno del poema sinfónico “En el temple i el cec de naixement”, de l’òpera
Jesús de Natzaret, de Josep Soler. Y también con la idea de cambio, Metamorphosen de Richard Strauss, Lontano de György Ligeti o a la ya citada Turangalîla de Messiaen, que inaugurará la temporada, si se puede.





							El violonchelista británico Sheku Kanneh-Mason interpretará con su hermana mayor, la pianista Isata Kanneh-Mason, sonatas de Bridge y Britten
										(Jake Turney)
						




La temporada de cámara traerá al histórico Hagen Quartett, que celebra sus 40 y a Daniel Barenboim en las Variaciones Diabelli
 de Beethoven, o a Andrè Schuen en La bella molinera de Schubert. Y como solistas –a parte de los Casals, que debutan en como cuarteto concertante con la OBC– acudirán pianistas como el de Pierre-Laurent Aimard, Katia Buniatishvili y Josep Colom, las sopranos Iréne Theorin, Dorothea Röschmann y Núria Rial, el barítono Ian Bostridge, los violinistas Viktoria Mullova, el gran Pinchas Zukerman y Leticia Moreno, el viola Antoine Tamestit, el violonchelista Narek Hakhnazaryan así como Sol Gabetta y el joven británico Sheku Kanneh-Mason, o el flautista Emmanuel Pahud. 


Grandes nombre internacionales
Barenboim tocará las ‘Variaciones Diabelli’ y Gergiev dirigirá la Filarmónica de Múnich con Janine Jansen en Sibelius


El ciclo de Orquestas Internacionales, coproducido con Ibercàmera y BCN Clàssics, arranca en septiembre con la 5ª de Chaikovski por la Filarmónica de Munich que dirigirá Gergiev y con Janine Jansen defendiendo el Concierto para violín
de Sibelius.
En noviembre, la orquesta de Weimar interpretará –novedad nacional– la pieza que Lorin Maazel compuso en los ochenta sobre El anillo del nibelungo (15 horas de ópera resumidas en 90 minutos de música sinfónica). En el 2021, la Sinfónica de Radio Viena y Marin Alsop ofrecerán la 7ª de Chaikovski, escrita bajo las bombas durante el sitio de Leningrado. Y la orquestra de Stuttgart hará La canción de la tierra de Mahler. Y dos citas más que ya anunció BCN Clàssics: la orquesta del Concertgebouw con Paavo Jarvi y Yuja Wang, y la Sinfónica de Tokio con Jonathan Nott dirigiendo Mahler.


Público joven
Nueva tarifa plana para menores de 35: acceso a toda la temporada por 85 euros. Los menores de 25 siguen en 50


En cuanto a creación de públicos y dado que la tarifa plana para menores de 25 ha tenido éxito (50 euros por acceso ilimitado, y ha atraído a 340 jóvenes), L’Auditori añade otra ahora, la Tarifa Plana-35, que da acceso a menores de 35 a toda la temporada por 85 euros.

























 







Normas de participación




Powered by Livefyre











Al minuto
Athletic Bilbao - Atlético de Madrid: Liga Santander en directo
Esta diseñadora gráfica ha versionado las aplicaciones y programas más famosos como si fuesen retro
El Ayuntamiento de Barcelona podría sancionar a los bañistas que incumplan las normas con multas de hasta 200 euros
Sibilinas trampas en la cocina del infierno
Coronavirus en Zaragoza: Última hora de la fase 3 de la desescalada en Aragón
Intentan robar en Ciutat Vella armados con palos y botellas




















Figo vuelve a la carga contra el Gobierno y esta vez se ceba con Fernando Simón




Preocupación por Matthew Perry, desmejorado y con muchos kilos de más













