#!/usr/bin/env python3

from fuzzywuzzy import fuzz
import os
import re
from urllib.request import urlopen
import os
import requests
from bs4 import BeautifulSoup
from urllib.parse import urlparse
from extruct.jsonld import JsonLdExtractor
import json
from xml.dom.minidom import parseString

CATALANISMOS_PATH = os.path.join(os.getcwd(), "catalanismos.txt")
RESULTS_PATH = os.path.join(os.getcwd(), "results.csv")
FIELDS = ["newspaper", "article", "date", "word", "catalanismo", "match_confidence", "line_number", "context"]
ARTICLES_PATH = os.path.join(os.getcwd(), "newspapers")
URLS_PATHS = os.path.join(os.getcwd(), "urls.txt")

def process_catalanismos(article):
    with open(article) as articleFile:
        lineN = 1
        parsedName = os.path.basename(articleFile.name).split("%%%")
        newspaperName = parsedName[0]
        articleDate = parsedName[1]
        articleName = parsedName[2]
        for line in articleFile:
            parsedLine = line.replace("\n", '')
            parsedLine = re.sub('\<.*?\>', '', parsedLine)
            for word in parsedLine.split():
                matched, catalanismo, confidence = match_catalanismo(word)
                data = [newspaperName, articleName, articleDate, word, catalanismo, confidence, lineN, parsedLine]
                if matched:
                    fieldCount = 0
                    for fieldData in data:
                        resultsFile.write("\"" + str(fieldData).rstrip("\n") + "\"")
                        if (fieldCount != len(data) - 1):
                            resultsFile.write(",")
                        fieldCount += 1
                    resultsFile.write("\n")

            lineN += 1

def match_catalanismo (word):
    preparsedWord = word.strip()
    preparsedWordLower = preparsedWord.lower()
    for catalanismo in CATALANISMOS:
        ratio = fuzz.ratio(catalanismo, preparsedWordLower)
        if ratio > 80:
            return True, catalanismo, ratio

    return False, "", ratio
   
def extract_values(obj, key):
    """Pull all values of specified key from nested JSON."""
    arr = []

    def extract(obj, arr, key):
        """Recursively search for values of key in JSON tree."""
        if isinstance(obj, dict):
            for k, v in obj.items():
                if isinstance(v, (dict, list)):
                    extract(v, arr, key)
                elif k == key:
                    arr.append(v)
        elif isinstance(obj, list):
            for item in obj:
                extract(item, arr, key)
        return arr

    results = extract(obj, arr, key)
    return results

def downloadURLs():
    # Create target directory & all intermediate directories if don't exists
    try:
        os.makedirs(ARTICLES_PATH)    
    except FileExistsError:
        pass

    with open(URLS_PATHS) as urlsFile:
        for url in urlsFile:
            newspaperName = '{uri.netloc}'.format(uri=urlparse(url))
            html_text = requests.get(url).text
            soup = BeautifulSoup(html_text, 'html.parser')
            print ("Newspaper: " + newspaperName)
            jslde = JsonLdExtractor()
            data = jslde.extract(html_text)
            if (newspaperName == "www.elperiodico.com"):
                id = "ep-detail-body"
                datePublished = extract_values (data, "datePublished")[0]
                title = soup.title.string
            elif (newspaperName == "www.elnacional.cat"):
                id = "article-body"
                datePublished = extract_values (data, "datePublished")[0]
                title = soup.title.string
            elif (newspaperName == "www.lavanguardia.com"):
                id = "detail__article"
                datePublished = [meta.get('content') for meta in soup.find_all('meta', itemprop='datePublished')][0]
                title = soup.find_all('title')[0].string
            
            print ("Title: " +  title)

            result = soup.body.find("div", attrs={'class':id})
            with open(os.path.join(ARTICLES_PATH, newspaperName + "%%%" + datePublished + "%%%" + title), "w+") as resultFile:
                resultFile.write(result.text)

if __name__ == '__main__':
    # Download all articles
    downloadURLs()

    with open(CATALANISMOS_PATH) as catalanismosFile:
        CATALANISMOS = [x.strip().lower() for x in catalanismosFile]
        print (CATALANISMOS)

    # Start from scratch
    global resultsFile
    resultsFile = open(RESULTS_PATH,"w+")
    for field in FIELDS:
        resultsFile.write(field)
        if (field != FIELDS[len(FIELDS)-1]):
            resultsFile.write(",")
    resultsFile.write("\n")
    resultsFile.close()

    resultsFile = open(RESULTS_PATH, "a")

    with os.scandir(ARTICLES_PATH) as articles:
        for article in articles:
            print (article.name)
            process_catalanismos(article)

